﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assesment
{
   


    internal class Developer
    {
        public int id;
        public string name;
        public DateTime joiningDate;
        public string projectAssigned;
        
        public virtual void GetDetails()
        {
            Console.WriteLine("Enter The id:");
            id=int.Parse(Console.ReadLine());
            Console.WriteLine("Enter The Name:");
            name = Console.ReadLine();
            Console.WriteLine("Enter the joining date:");
            joiningDate = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Enter the project Assigned:");
            projectAssigned = Console.ReadLine();
        }
        public void DisplayDetails()
        {
            Console.WriteLine("The Id is {0}",id);
            Console.WriteLine("The name of the developer is {0}",name);
            Console.WriteLine("The joining date of Developer is {0}",joiningDate);
            Console.WriteLine("The Project Assigned to the developer is {0}",projectAssigned);
        }
    }
}
