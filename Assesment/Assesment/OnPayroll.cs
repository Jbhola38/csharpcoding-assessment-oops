﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assesment
{
    internal class OnPayroll: Developer
    {
        public string dept;
        public string manager;
        public double netSalary;
        public byte exp;
        public  double finalSalary;

        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter the Department:");
            dept = Console.ReadLine();
            Console.WriteLine("Enter the Manager Name");
            manager = Console.ReadLine();
            Console.WriteLine("Enter the Salary");
            netSalary = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter the experience");
            exp=byte.Parse(Console.ReadLine());
            Calculatepayment(netSalary, exp);
            DisplayDetails();
        }
        public void Calculatepayment(double netSalary,byte exp)
        {
            if (exp > 10)
            {
                finalSalary = (((10 / 100) * netSalary) + ((8.5 / 100) * netSalary) - 6500+netSalary);
            }
            else if (exp > 7 && exp < 10)
            {
                finalSalary = (((7 / 100) * netSalary) + ((6.5 / 100) * netSalary) - 4100+netSalary);
            }
            else if (exp > 5 && exp < 7)
            {
                finalSalary = (((4.1 / 100) * netSalary) + ((3.8 / 100) * netSalary) - 1800+netSalary);
            }
            else
            {
                finalSalary = (((1.9 / 100) * netSalary) + ((2.0 / 100) * netSalary) - 1200+netSalary);
            }

        }
        public void DisplayDetails()
        {
            Console.WriteLine("The Basic Salary is {0}",netSalary);
            Console.WriteLine("The Total salary is {0}",finalSalary);
        }
    }
}
