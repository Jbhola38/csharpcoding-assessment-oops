﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assesment
{
    internal class OnContract:Developer
    {
        public int duration;
        public double chargesPerDay;
        public double paymentAmount;
        public OnContract():base()
        {

        }
        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter the Duration of Contract in days");
            duration = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Charges per Day:");
            chargesPerDay = Convert.ToDouble(Console.ReadLine());
            CalculatePayment();
        }
        public void CalculatePayment()
        {
            paymentAmount = chargesPerDay * duration;
            DisplayDetails();
        }
        public void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("The contract Duration is: {0}  days ",duration);
            Console.WriteLine("The charges perDay is: {0}",chargesPerDay);
            Console.WriteLine("Total Payment done is: {0}",paymentAmount);
        }
    }
}
